\name{get_maximum}
\alias{get_maximum}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
A function that should be used in final to get all results with respect to estimates of the proportion of mapped reads from a FASTQ file
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
A program that for each reference or barcode provides estimates of the proportion of mapped reads from a FASTQ file
}
\usage{
get_maximum(rds, ref, mis_match = 2, order_by_proportion = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{rds}{
%%     ~~Describe \code{rds} here~~
a string, the directory of a FASTQ file containing all reads
}
  \item{ref}{
%%     ~~Describe \code{ref} here~~
a string, the directory of a FASTA file containing all references or barcodes
}
  \item{mis_match}{
%%     ~~Describe \code{mis_match} here~~
an integer, allow how many mismatches or mismatch tolerance when we filter all references or barcodes
}
  \item{order_by_proportion}{
%%     ~~Describe \code{order_by_proportion} here~~
a boolean, it is TRUE when you want the final table of each reference or barcode with estimates of the proportion of mapped reads sorted by the proportions decreasingly, while it is FALSE when you want the final table of each reference or barcode with estimates of the proportion of mapped reads sorted by the sequence names
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
It will call all other functions from package yingzhou, including P_error,readFiles, Q, hamming, joint_prbbs
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
It will return a list including table1, table2, the number of unmapped reads, the number of mapped reads and a plot
table1 is a table including each read name, each read sequence, mapped reference sequence, mapped reference name, and maximum probability
table2 is a table including each reference name and its proportion of mapped reads
The plot shows the top 50 references with respect to mapped reads proportions
}
\references{
%% ~put references to the literature/web site here ~
H. Li, J. Ruan, and R. Durbin.Mapping short DNA sequencing reads and calling variants using mapping quality scores. Genome Res., 18:1851, 2008.
}
\author{
%%  ~~who you are~~
Yingzhou Liu
}

\examples{
rds_dir = '~/Desktop/824_2/bios824-notebook-spring2019/my.gen.fastq'
ref_dir = '~/Desktop/824_2/bios824-notebook-spring2019/my.gen.fasta'
x = get_maximum(rds_dir, ref_dir, 2, TRUE)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~reads}% use one of  RShowDoc("KEYWORDS")
\keyword{ ~references }% __ONLY ONE__ keyword per line
