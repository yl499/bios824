\name{readFiles}
\alias{readFiles}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
read in fastq and fasta files
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
read in fastq and fasta files
return a vector including sequence identifiers and a vector including sequence letters for fasta files
return a vector including sequence identifiers, a vector including sequence letters, a vector including same sequence identifiers and a vector including quality values for fastq files
}
\usage{
readFiles(rds, ref)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{rds}{
%%     ~~Describe \code{rds} here~~
the directory of FASTQ including all reads
}
  \item{ref}{
%%     ~~Describe \code{ref} here~~
the directory of FASTA including all references
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
return a vector including sequence identifiers and a vector including sequence letters for fasta files
return a vector including sequence identifiers, a vector including sequence letters, a vector including same sequence identifiers and a vector including quality values for fastq files
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
return a vector including sequence identifiers and a vector including sequence letters for fasta files
return a vector including sequence identifiers, a vector including sequence letters, a vector including same sequence identifiers and a vector including quality values for fastq files
}

\author{
%%  ~~who you are~~
Yingzhou Liu
}

\examples{
rds_dir = '~/Desktop/824_2/bios824-notebook-spring2019/my.gen.fastq'
ref_dir = '~/Desktop/824_2/bios824-notebook-spring2019/my.gen.fasta'
readFiles(rds_dir, ref_dir)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~reads }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~references }% __ONLY ONE__ keyword per line
