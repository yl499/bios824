joint_prbbs <- function(read, ref, score){
  read <- strsplit(read,"")[[1]]
  L <- length(read)
  ref <- strsplit(ref,"")[[1]]
  score <- strsplit(score,"")[[1]]
  prob <- c()
  for(i in 1:L){
    if(read[i] == ref[i]){
      prob[i] <- 1 - P_error(Q(score[i]))
    }
    else{
      prob[i] <- P_error(Q(score[i])) / 3
    }
  }
  return(prod(prob))
}
